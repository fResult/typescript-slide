<div align="center">
  <a href="08-generic-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="10.what-is-next.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Introduction to Object-Oriented Programming (OOP)
## Object-Oriented Programming: OOP
**OOP** หรือ **การเขียนโปรแกรมเชิงวัตถุ** คือ  
การเขียนโปรแกรมโดยการมองทุกอย่างในโปรแกรมให้เป็นวัตถุ  
(โดยต่อไปนี้ผมขอใช้คำว่า ***object*** แทนคำว่า ***วัตถุ*** ไปเลยนะครับ)

> ### Table of Contents
>> - [Classes & Objects](#classes-&-objects)
>>    - [Classes](#classes) 
>>    - [Objects](#objects)
>> - [4 Principles of OOP](#4-หลักการสำคัญของ-oop)
>>    1. [Encapsulation](#1.-encapsulation)
>>    2. [Abstraction](#2.-abstraction)
>>    3. [Polymorphism](#3.-polymorphism)
>>    4. [Inheritance](#4.-inheritance)

## Classes & Objects
### Classes
โดยแนวคิดแล้ว **Class** คือ Entity (เอกลักษณ์) ของสิ่งต่างๆ ที่มีอยู่ในชีวิตจริง   
ขอยกตัวอย่าง เช่น...  

**Entity ของรถยนต์ (Car)**  
*โดยทั่วไปแล้ว* รถยนต์ทุกคัน ต้องมีล้อ ประตู เกียร์(Transmission) ที่นั่ง ฯลฯ  
และมีพฤติกรรม เดินหน้า ถอยหลัง เลี้ยวซ้าย เลี้ยวขวา เพิ่มเกียร์ ลดเกียร์ ฯลฯ ได้

<details><summary><b>Example code</b></summary>

```typescript
class Car {
  constructor(
    private doors: number,
    private seats: number,
    private transmission: number,
    private currentTransmission: number,
    private wheels: number,
  ) {
  }

  moveForward() {
    console.log('Car moves forward.')
  }

  moveBackward() {
    console.log('Car moves backward')
  }

  turnLeft() {
    console.log('Car turns left')
  }

  turnRight() {
    console.log('Car turns right')
  }

  upSpeed() {
    this.currentTransmission += 1
    console.log(`Current transmission is ${this.currentTransmission}`)
  }

  downSpeed() {
    this.currentTransmission -= 1
    console.log(`Current transmission is ${this.currentTransmission}`)
  }
}
```

</details>

##### คราวนี้ลองมาดูตัวอย่างที่ใกล้เคียงกับการการทำงานจริงกันบ้าง  

**Entity ของ พนักงานบริษัท (Employee) ในระบบบัญชีเงินเดือน**  
*โดยทั่วไปแล้ว* พนักงานทุกคน อาจมี**ข้อมูล**  
ชื่อ-นามสกุล เงินเดือน เงินที่ได้จากการทำงานล่วงเวลา ฯลฯ  
และมี**พฤติกรรม**...  
คำนวณรายได้รวมพนักงาน และ สรุปข้อมูล (summary) ฯลฯ

<details><summary><b>Example code</b></summary>

```typescript
class Employee {
  constructor(
    private fullName: string,
    private salary: number,
    private ot: number
  ) {}

  private calculateTotalSalary(): number {
    return this.salary + this.ot
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary +
      '\nOvertime income: ' + this.ot +
      '\nTotal salary: ' + this.calculateTotalSalary()
    )
  }
}
```

</details>


**ดังนั้น** การนิยาม Class จึงเป็นการนิยามขึ้นด้วยมุมมอง**แบบอุปนัย (Induction)**  
โดยการสังเกตคุณสมบัติและพฤติกรรมโดยทั่วไปของกลุ่มของ object ที่เราสนใจ (Domain)  
ว่ามีคุณสมบัติ (Properties) และ พฤติกรรม (Methods) อะไรที่เหมือนๆ กันบ้าง  
แล้วจึงนำคุณสมบัติและพฤติกรรมเหล่านั้นมาเป็นส่วนหนึ่งของ Class นั่นเองครับ  

หรือถ้าพูดง่ายๆ ก็คือ...  
การนิยาม Class นิยามได้จากการสังเกต object ที่อยู่ในกลุ่มเดียวกัน  
แล้วสรุปรวบยอดออกมาเป็น Class ของกลุ่ม object ดังกล่าว

> Note:
>> **อุปนัย** หรือ **Induction** หมายถึง  
>> การนำข้อมูลของสิ่งที่เรารู้แล้วบางอย่าง มาสรุปเป็นความรู้ทั้งหมด

### Objects
**Object ในมุมมองของการเขียนโปรแกรมเชิงวัตถุ หรือ OOP** คือ  
สิ่งที่เราสร้างขึ้นมาจาก Class ที่เรา(หรือ library อื่น) เขียนไว้  
Object ใดๆ ที่ถูกสร้างขึ้นจาก Class ไหน จะมีคุณสมบัติและพฤติกรรมของคลาสนั้นไปโดยปริยาย  

#### ตัวอย่าง: 
<details><summary><b>แผนภาพแสดงการสร้างออบเจ็กต์ จากคลาส `Employee`</b></summary>

![ภาพแสดงการสร้างออบเจ็กต์ของพนักงาน 3 คน จากคลาส `Employee` ในระบบบัญชีเงินเดือน](images/009-01.png)

</details>

จากตัวอย่างของคลาส `Employee` ที่กล่าวถึงในหัวข้อ `Classes` ข้างต้น  
เราสามารถสร้าง Object ของพนักงาน 3 คน ได้ดังนี้

```typescript
const emp1 = new Employee('John Wick', 300_000, 200_000)
const emp2 = new Employee('Thomas Anderson', 1_200_000, 0)
const emp3 = new Employee('John Constantine', 250_000, 50_000)
```

เมื่อสร้าง Object เสร็จแล้ว เราสามารถเรียกใช้งาน method (หรือพฤติกรรมที่คลาส Employee ทำได้) ได้ดังนี้ครับ
```typescript
emp1.displaySummary()
console.log('-----')
emp2.displaySummary()
console.log('-----')
emp3.displaySummary()
```

#### ผลลัพธ์ของการเรียกใช้ method `displaySummary()` ของ object ที่สร้างจาก `Employee` class
![ภาพแสดงผลลัพธ์จากการเรียกใช้ method displaySummary() จากคลาส `Employee`](images/009-02.png)

---
แต่เดี๋ยวก่อน !

> การเขียนโปรแกรมเชิงวัตถุ อาจยังไม่ใช่ `Object-Oriented Programming` ก็ได้นะ

นี่คือสิ่งที่ผมเคยอ่านเจอในบทความหนึ่ง  
เพราะในภาษา Javascript แค่เรา สร้าง Javascript Object  
ก็พอจะเรียกได้ว่าเราเขียนโปรแกรมเชิงวัตถุแล้วนี่ ?
ดังนั้นเรามาดูกันหน่อย ว่า OOP ที่แท้จริงจะต้องมีหน้าตาเป็นอย่างไร

OOP ประกอบไปด้วย 4 หลักการสำคัญที่จะกล่าวถึงดังต่อไปนี้ครัชชชช

## 4 หลักการสำคัญของ OOP
> OOP ≠ การโปรแกรมเชิงวัตถุ

#### การเขียนโปรแกรมเชิงวัตถุ หรือ OOP มีหลักการสำคัญ 4 ข้อ ได้แก่
1. [Encapsulation - การซ่อนข้อมูล](#2.-encapsulation)
2. [Polymorphism - การมีหลายรูปแบบ](#4.-polymorphism)
3. [Inheritance - การสืบทอด](#3.-inheritance)
4. [Abstraction - ความเป็นนามธรรม](#1.-abstraction)

### 1. Encapsulation
เป็นหลักการห่อหุ้มข้อมูลที่อยู่ภายใน class ใดๆ 
เพื่อป้องกันการเข้าถึง (อ่านหรือเปลี่ยนแปลงค่า) ข้อมูลที่อยู่ในคลาสดังกล่าว  
ซึ่งการจะเข้าถึงข้อมูลได้ ต้องกระทำผ่าน Method ที่ class ได้ provided ไว้เท่านั้น  
...Method ที่ว่านี้ มักถูกเรียกว่า `Getter` / `Setter`

จาก Code ต่อไปนี้ เราห่อหุ้มข้อมูล `age` ที่อยู่ในคลาส `Person` เอาไว้  
และให้กำหนดค่า `age` ผ่าน method `setAge()` เท่านั้น  
เพื่อป้องกันการระบุอายุที่*มีค่าน้อยกว่า 0*  
```typescript
class Person {
  private age: number = 0

  constructor(age: number) {
    this.setAge(age)
  }

  setAge(age: number): void {
    if (age < 0) {
      throw new Error('Age must not less than 0.')
    }
    this.age = age
  }

  getAge(): number {
    return this.age
  }
}

const person1 = new Person(18)
console.log(person1.age) // this line will error because `age` is a private property
const person2 = new Person(-1) // this will error, too
```

> **Note:**
>> ระดับการเข้าถึงข้อมูล (Data Access Modifier) ใน Typescript มีอยู่หลายแบบ แต่ในที่นี้ขอนำเสนอ ทั้งหมด 3 แบบ คือ
>> 1. **Public** จะเข้าถึงข้อมูลจากภายนอกคลาสได้โดยไม่มีข้อยกเว้น (กรณีใช้ `public` modifier สามารถละคำว่า `public` เอาไว้ได้)
>> 2. **Private** จะเข้าถึงข้อมูลจากภายนอกคลาสมิได้ จะเข้าถึงข้อมูลได้จากภายในตัวคลาสเองเท่านั้น
>> 3. **Protected** จะเข้าถึงข้อมูลได้จากคลาสที่สืบทอด (inherit) จากตัวคลาสปัจจุบัน หรือเข้าถึงข้อมูลภายในตัวคลาสเอง  
>>
> [ศึกษา Access Modifiers (Member Visibility) เพิ่มเติมที่ Typescriptlang.org](https://www.typescriptlang.org/docs/handbook/2/classes.html#member-visibility)

### 2. Polymorphism
เรื่องนี้มีรายละเอียดเยอะมาก ในที่นี้ผมขอนำเสนอไปที่ 2 หัวข้อ คือ Overloading และ Overriding เท่านั้นนะ
#### Overloading
คือ ในคลาสใดคลาสหนึ่ง มี method ที่มีชื่อซ้ำ 
แต่ Parameter มีจำนวนไม่เท่ากัน หรือ จำนวนเท่ากันแต่ Data type ของ Param ไม่เหมือนกัน

<details><summary><b>Example code</b></summary>

```typescript
class Employee {
  constructor(public fullName: string, public salary: number, public ot: number) {
    
  }

  calculateTotalSalary(): number {
    return this.salary + this.ot
  }
  
  displayInfo(fullName: string): void {
    console.log('Full Name: ' + fullName)
  }
  
  displayInfo(salary: number): void {
    console.log('Salary: ' + salary)
  }
  
  displayInfo(fullName: string, salary: number, ot: number, totalSalary: number): void {
    console.log(
      'Full Name: ' + fullName +
      '\nSalary: ' + salary +
      '\nOT Income: ' + ot +
      '\nTotal salary: ' + totalSalary
    )
  }
}

const emp = new Employee('Thomas Anderson', 1_200_000, 0)
console.log(emp.displayInfo(emp.fullName))
console.log(emp.displayInfo(emp.salary))
console.log(emp.displayInfo(emp.fullName, emp.salary, emp.ot, emp.calculateTotalSalary()))
```

</details>

#### Overriding
คือ เมื่อคลาสลูกสืบทอด (inherit) คลาสแม่ แล้วมีการนำ method ของแม่มาเขียนใหม่ เพื่อเปลี่ยนแปลงการทำงานให้แตกต่างจากเดิม

<details><summary><b>Example code</b></summary>

```typescript
class Employee {
  constructor(
    protected fullName: string,
    protected salary: number,
    protected ot: number
  ) {}

  protected calculateTotalSalary(): number {
    return this.salary + this.ot
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      ',\nBase salary: ' + this.salary +
      ',\nOvertime income: ' + this.ot +
      ',\nTotal salary: ' + this.calculateTotalSalary()
    )
  }
}

class CasualEmployee extends Employee {
  private withholdingTax: number = 0.03

  calculateTotalSalary(): number {
    return (this.salary + this.ot) * (1 - this.withholdingTax)
  }

  // Overriding method
  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      ',\nBase salary: ' + this.salary.toLocaleString() +
      ',\nOvertime income: ' + this.ot.toLocaleString() +
      ',\nWithholding tax: ' + this.withholdingTax * 100 + '%' +
      ',\nTotal salary: ' + this.calculateTotalSalary().toLocaleString()
    )
  }
}
```

</details>

### 3. Inheritance
เป็นหลักการสืบทอดคลาส จาก Super class ไปสู่ Sub class  
หรือจาก คลาสแม่ สู่ คลาสลูก  
ผลที่เกิดขึ้นจากการสืบทอดคลาสก็คือ  
คลาสลูกจะมี properties และ methods ทั้งหมดที่คลาสแม่มีอยู่

#### ตัวอย่าง คลาส CasualEmployee และ PermanentEmployee ได้สืบทอดคลาสจาก Employee ดังนี้

<details><summary><b>แผนภาพแสดงการสืบทอดจากคลาส `Employee`</b></summary>

![แผนภาพแสดงการสืบทอดจากคลาส `Employee`](images/009-03.png)

</details>

```typescript
class Employee {
  constructor(
    protected fullName: string,
    protected salary: number,
    protected ot: number
  ) {}

  protected calculateTotalSalary(): number {
    return this.salary + this.ot
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary +
      '\nOvertime income: ' + this.ot +
      '\nTotal salary: ' + this.calculateTotalSalary()
    )
  }
}

class CasualEmployee extends Employee {
  private withholdingTax: number = 0.03

  protected calculateTotalSalary(): number {
    return (this.salary + this.ot) * (1 - this.withholdingTax)
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary.toLocaleString() +
      '\nOvertime income: ' + this.ot.toLocaleString() +
      '\nWithholding tax: ' + `${this.withholdingTax * 100}%` +
      '\nTotal salary: ' + this.calculateTotalSalary().toLocaleString()
    )
  }
}

class PermanentEmployee extends Employee {
  protected meetingAllowance: number = 0

  constructor(
    fullName: string,
    salary: number,
    ot: number,
    meetingAllowance: number
  ) {
    super(fullName, salary, ot)
    this.meetingAllowance = meetingAllowance
  }

  calculateTotalSalary():number {
    return this.salary + this.ot + this.meetingAllowance
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary.toLocaleString() +
      '\nOvertime income: ' + this.ot.toLocaleString() +
      '\nMeeting allowance: ' + this.meetingAllowance.toLocaleString() +
      '\nTotal salary: ' + this.calculateTotalSalary().toLocaleString()
    )
  }
}

const casualEmp = new CasualEmployee('Neo TheOne', 3_000_000, 1_500_000)
casualEmp.displaySummary()

console.log('-----')

const permEmp = new PermanentEmployee('John Wick', 500_000, 1_500_000, 450_000)
permEmp.displaySummary()
```

### 4. Abstraction
คือการนำความเป็นนามธรรมมาใช้ในโปรแกรม  
โดยคลาสใดๆ ที่ implement interface ที่เรานิยามขึ้นในโปรแกรม  
คลาสนั้นๆ ต้องมี properties และ methods ทั้งหมดที่มีอยู่ใน interface ดังกล่าวด้วย  
(เหมาะกับการใช้ขึ้นโครงให้กับ class ที่จะมา implement)

#### ตัวอย่าง การ implement interface ตามหลักการ abstraction

<details><summary><b>แผนภาพแสดงการ implement อินเตอร์เฟส `EmployeeImpl`</b></summary>

![แผนภาพแสดงการ implement อินเตอร์เฟส `EmployeeImpl`](images/009-04.png)

</details>

#### Code Example
```typescript
interface EmployeeImpl {
  fullName: string
  salary: number
  ot: number

  calculateTotalSalary(): number

  displaySummary(): void
}

class CasualEmployee implements EmployeeImpl {
  fullName: string
  salary: number
  ot: number
  private withholdingTax: number = 0.03

  constructor(fullName: string, salary: number, ot: number) {
    this.fullName = fullName
    this.salary = salary
    this.ot = ot
  }


  calculateTotalSalary(): number {
    return (this.salary + this.ot) * (1 - this.withholdingTax)
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary.toLocaleString() +
      '\nOvertime income: ' + this.ot.toLocaleString() +
      '\nWithholding tax: ' + `${this.withholdingTax * 100}%` +
      '\nTotal salary: ' + this.calculateTotalSalary().toLocaleString()
    )
  }
}

class PermanentEmployee implements EmployeeImpl {
  protected meetingAllowance: number = 0

  constructor(
    public fullName: string,
    public salary: number,
    public ot: number,
    meetingAllowance: number
  ) {
    this.meetingAllowance = meetingAllowance
  }


  calculateTotalSalary():number {
    return this.salary + this.ot + this.meetingAllowance
  }

  displaySummary(): void {
    console.log(
      'Name: ' + this.fullName +
      '\nBase salary: ' + this.salary.toLocaleString() +
      '\nOvertime income: ' + this.ot.toLocaleString() +
      '\nMeeting allowance: ' + this.meetingAllowance.toLocaleString() +
      '\nTotal salary: ' + this.calculateTotalSalary().toLocaleString()
    )
  }
}

const casualEmp = new CasualEmployee('Neo TheOne', 3_000_000, 1_500_000)
casualEmp.displaySummary()

console.log('-----')

const permEmp = new PermanentEmployee('John Wick', 500_000, 1_500_000, 450_000)
permEmp.displaySummary()
```

เรื่อง OOP ผมขอตัดจบแบบปาหมอนแต่เพียงเท่านี้แล้วกัน  
ต่อไปลองมาดูกันว่าเราจะเอา Typescript ไปใช้งานกับ Node และ React ได้ยังไง  
[>>> จิ้มแรงๆ ที่นี่ <<<](10.what-is-next.md) ได้เรยยยย

---

<div align="center">
  <a href="08-generic-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="10.what-is-next.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />
