<div align="center">
  <a href="05-union-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="07-function-as-type.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Type Aliases & Custom Types
## Table of contents
> - [Type Aliases](#type-aliases)
> - [Object Types](#object-types)
> - [Nested Object Types](#nested-object-types)

## Type Aliases

เป็นการตั้งชื่อเล่นให้กับ type เพื่อความสะดวกในการเรียกใช้งาน

#### ตัวอย่าง

```typescript
type text = string
let anotherPhrase: text = 'Goodbye'
let phrase: string = 'Hello'
```

![ภาพแสดงการประกาศ type alias](images/006-01.png)  
จากภาพ ในกรอบสีฟ้าอันแรก เป็นการประกาศ type alias ให้กับ `string` เรียกว่า `text`
และกรอบสีฟ้าอันที่สอง เป็นการใช้งาน type `text` (ซึ่งมีค่าเท่ากับ type `string`) ที่ได้ประกาศไว้ก่อนหน้านั้น

> ### Note that...
> > เราสามารถใช้ type `text` แทน string ได้เลย
> > ในขณะที่ type `string` ยังคงใช้ได้เหมือนเดิม

### ลองกำหนดค่า `123` ให้กับตัวแปร `anotherPhrase`

```typescript
type text = string
let anotherPhrase: text = 'Goodbye'
anotherPhrase = 123
```

จะเกิด Error ว่า Type `number` is not assignable to type `string`.  
เพราะว่า `text` ก็คือ type alias ของ `string` นั่นเอง

![ภาพแสดงการ assigned number value ให้กับ type text ที่สร้างก่อนหน้านั้น](images/006-02.png)

#### อีกตัวอย่างหนึ่ง

```typescript
type StringOrNumber = string | number   // Declare type alias

                     // Use type alias 
function addFive(thing: StringOrNumber) {
  return (typeof thing === 'string') ? thing + 5 : +thing + 5
}
```

> นอกจากการกำหนด type alias อย่างง่ายๆ แบบด้านบนได้แล้ว  
> เรายังสามารถนำไปใช้กับข้อมูลที่ซับซ้อนขึ้นได้ ตามที่จะพูดถึงต่อไปนี้...

## Object Types
```typescript
type Person = {
  firstName: string
  lastName: string
  age: number
} | null // Union with `null`

const foundPerson: Person = { firstName: 'Sila', lastName: 'Setthakan-anan', age: 18 }
const notFoundPerson: Person = null // Assume that not found from database

function describePerson(person: Person): void {
  if (!person) {
    throw new Error('Null Pointer Error!')
  }
  const { firstName, lastName, age } = person
  console.log([firstName, lastName, age].join(', '))
}

try {
  describePerson(foundPerson)
  describePerson(notFoundPerson)
} catch (err) {
  console.error(err.message)
}
```
 ![ภาพแสดงตัวอย่างการนำ type Person ไปใช้](images/006-03.png)  

จากนั้นให้ run `nodemon type-aliases.js`
 ![ภาพแสดงตัวอย่าง](images/006-04.png)

### Nested Object Types
```typescript
// Declare Address type
type Address = {
  houseNo: string
  building?: string
  moo?: string
  street: string
  subDistrict: string
  district: string
  province: string
  postalCode: string
} | null // Union with `null`

// Declare Customer type
type Customer = {
  firstName: string
  lastName: string
  address: Address
} | null // Union with `null`

const addr1 = {
  houseNo: '123/45',
  moo: 'Moo 4',
  street: 'Nimman Haemin',
  subDistrict: 'Sutep',
  district: 'Muang Chaiang Mai',
  province: 'Chaiang Mai',
  postalCode: '50200'
}
const customer1 = {
  firstName: 'George',
  lastName: 'Makeslifebetter',
  address: addr1 // `address` property is an `Address` type
}

// Use Customer (include Address) type
function displayCustomer(cust: Customer) {
  console.log(cust)
}
displayCustomer(customer1)
```

#### ลองเปลี่ยน Address เป็นข้อมูล type string
```typescript
const customer1 = {
  firstName: 'George',
  lastName: 'Makelifebetter',
  // Changed Address value
  address: '123/45, Moo 4, Nimman Haemin, Sutep, Sutep, Muang Chaiang Mai, Chaiang Mai, 50200'
}

function displayCustomer(cust: Customer) {
  console.log(cust)
}

displayCustomer(customer1)
```
เราจะพบ Error ตามข้างล่างนี้ เพราะว่า `address` property ที่อยู่ใน `customer1`มี type เป็น `string`  
ในขณะที่ `Customer` type มี `address` property เป็น `Address` type นั่นเอง  

 ![ภาพแสดงตัวอย่าง error ที่เกิดจากการระบุ Address value ไม่ตรงกับ type Address ที่ประกาศไว้](images/006-05.png)

---

<div align="center">
  <a href="05-union-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="07-function-as-type.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />
