<div align="center">
  <a href="04-basic-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="06-type-aliases-n-custom-type.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Union Types

ในบางครั้งเราก็จำเป็นต้องใช้งานตัวแปร หรือ Parameter ที่อาจมี type ได้มากกว่า 1 อย่าง  
(ซึ่งหมายถึงในขณะหนึ่งๆ ตัวแปรหรือ parameter นั้นๆ จะเป็นได้ แค่ 1 type เท่านั้นนะ)

```typescript
function addFive(thing: string | number) {
  return (typeof thing === 'string') ? thing + 5 : +thing + 5
}

console.log(addFive('10'))  // 105
console.log(addFive(10))    // 15
```

### อีกหนึ่งในตัวอย่างที่มีการนำเรื่อง Union Type ไปใช้จริง
![ภาพแสดงตัวอย่างการใช้งาน Union type](images/005-01.png)  
จาก code ในภาพข้างบน ได้ทำกรอบแดงไว้ให้ดู ว่านั่นเป็นการกำหนด Union type

   1. สร้าง `useAsync` hook (`useAsync.tsx` file)
      ```tsx
      function useAsync<T, E>(
        asyncFn: (params?: any) => Promise<any>,
        immediate: boolean = true
      ) {
        const [value, setValue] = useState<T | Array<T> | null>(null)
        const [error, setError] = useState<E | null>(null)
        const [pending, setPending] = useState(false)
      
        const execute = useCallback(
          async (params?) => {
            setError(null)
            setValue(null)
            setPending(true)
      
            try {
              const { data }: { data: Array<T> | T } = await asyncFn(params)
              setValue(data)
            } catch (err) {
              setError(err)
              console.error('❌ Error', err?.message)
            } finally {
              setPending(false)
            }
          },
          [asyncFn]
        )
      
        useEffect(() => {
          if (immediate) {
            ;(async () => await execute())()
          }
        }, [execute, immediate])
      
        return { execute, value, pending, error }
      }
      ```
         
   2. การเรียกใช้ `useAsync` hook (ใน `CountriesPage.tsx`)
      ```tsx
      const {
        data: countries,
        execute: getAllCountries,
        pending,
        error
      } = useAsync(async (id: string) => {
        return await axios.get(`${process.env.baseURL}/countries/${id}`))
      }
      ```
---

<div align="center">
  <a href="04-basic-types.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="06-type-aliases-n-custom-type.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />
