<div align="center">
  <a href="01-introduction-to-typescript.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="03-type-using-n-casting.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Setting up Typescript

## ขั้นตอนการ setup `typescript` ใน Project directory
1. สร้าง Project directory ชื่อ `typescript-learning` และเข้าถึง directory ดังกล่าว
   ```
   mkdir typescript-learning && cd typescript-learning
   ```

2. Generate Node Package Module ใน Project directory
   ```npm
   npm init -y

   หรือ

   yarn init -y
   ```
   * แล้วจะเห็นว่ามีไฟล์ package.json ปรากฏขึ้นใน directory  
      ![ภาพแสดง Project structure หลังการ generate Node Package Module](images/002-01.png)

3. ติดตั้ง package `typescript` เพื่อเอาไว้ใช้ compile จาก `ts` เป็น `js` และ `nodemon` ไว้เพื่อรัน file `js` แบบ watch mode *(ช่วยให้ไม่ต้อง stop คำสั่ง node ทุกครั้งที่การอัพเดตไฟล์ js)*
   ```npm
   npm install -g typescript nodemon

   หรือ

   yarn global add typescript nodemon
   ```
4. ลองทดสอบด้วยการรัน Typescript กัน
   1. สร้างไฟล์ `main.ts` ใน root directory
   2. เขียน code ลงใน `main.ts` ตามนี้
      ```typescript
      console.log('Hello, Typescript')
      ```
   3. Compile typescript ให้เป็น Javascript ด้วยคำสั่ง
      ```npm
      tsc main.ts
      ```
      ![ภาพแสดง Project structure หลังการ compile ไฟล์ main.ts](images/002-02.png)
   4. รันไฟล์ `main.js` เพื่อให้ code ที่เขียนทำงาน
      ```npm
      nodemon main.js
      
      หรือ

      node main.js
      ```
      ![ภาพแสดงผลการรันไฟล์ main.js](images/002-03.png)

## ลองหน่อย...
### 1. ให้สร้างฟังก์ชัน add() ด้วย code ต่อไปนี้ แล้ว run ออกมาทาง terminal (console)
   ```typescript
   function add(num1: number, num2: number): number {
     return num1 + num2
   }

   console.log('The result is:', add(4, 5)
   ```
   * รันไฟล์ `main.js` แล้วอยากเห็นผลลัพธ์ (ในกรอบสีแดง) ประมาณนี้  
      ![ภาพแสดงผลการรันไฟล์ main.js](images/002-04.png)
   
---

ถ้ารันแล้วได้ผลลัพธ์ดังภาพข้างบน ก็ขอให้ดีใจได้เลยว่า เรา setup TypeScript เสร็จเรียบร้อยแล้วครับ  
เราไปต่อที่ [เรื่อง Type Using & Type Casting](03-type-using-n-casting.md) กันเลยดีกว่า

---

<div align="center">
  <a href="01-introduction-to-typescript.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="03-type-using-n-casting.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />
