<div align="center">
  <a href="07-function-as-type.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="09-oop.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Generic Types
> ### Table of Contents
>> - [Generic Type usage](#ตัวอย่างการใช้งาน-generic-types)
>> - [Create Generic Functions](#create-generic-functions)
>> - [Create Generic Classes](#create-generic-classes)



** Generic Type** คือ Type แบบทั่วไป  
ซึ่งเป็นการนิยาม Type ของตัวแปร หรือ Parameter  
ไว้อย่างกว้างๆ ทำให้ ตัวแปรหรือ Param นั้นกลายเป็น Type ใดๆ ก็ได้

> โดย Generic Type จะช่วยเราในการลด Bug ที่มักเกิดจากการใช้ `any`  
(type `any` เทียบได้กับการใช้ตัวแปร หรือ Param โดยไม่ระบุ type ใดๆ  
เช่นเดียวกับภาษา Javascript)

แล้วทำไมต้อง Generics ?  
ทำไมไม่ใช้ `any` ซะเลยล่ะ

... ก็บอกไปตั้งแต่ต้นแล้วไงล่ะว่า  
`any` มันทำให้ Parameter หรือตัวแปรใดๆ นั้น dynamic มากเกินไป  
และนอกจากนี้ ด้วยความที่ Param มี type เป็น `any`  
ทำให้ Param นั้นสูญเสียความสามารถของการเป็น instance  
(พูดง่ายๆ ก็คือ เวลาที่เราพิมพ์ `.` ต่อท้ายชื่อ Param ใดๆ  
แล้วทำให้ IDE จะไม่ช่วย guide ต่อให้ว่ามี properties หรือ methods อะไรให้ใช้ได้อีกบ้าง)

#### เดี๋ยวเราลองมาดู code นี้นะ
```typescript
function addAny(n1: any, n2: any): any {
  n1 = '2'
  return n1 + n2
}

console.log(addAny(2, 3))  // 23
```

#### ทีนี้เรามาดูแบบ Generic กันบ้าง
```typescript
function addGeneric<T>(n1: T, n2: T): T {
  n1 = '2'    // Error this line
  return n1 + n2 // should add more handling before return
}

console.log(addGeneric(2, 3))
```
![ภาพแสดงตัวอย่าง Error เมื่อพยายาม assign string ให้กับ Generics](images/008-01.png)  
จากภาพ จะเห็นได้ว่า เราไม่สามารถนำ `string`  
มา assign ให้กับ parameter type `T` ซึ่งเป็น Generic ได้  
เพราะว่าตัวแปร generic อาจไม่ใช่ `string` ก็ได้ครับ  

ซึ่งจุดนี้เอง ที่ Generic จะเป็นตัวช่วยกอบกู้เราจาก Bug ได้อย่างปลอดภัยอีกครั้ง  
แม้ว่า Parameter ที่รับมาจะยังไม่ทราบ type ก็ตามแต่
- เราจะแทน Generic `T` ด้วยอะไรก็ได้ `A`, `B`, `C` ไปจนถึง `Z` ได้หมด  
  แต่ที่เลือกใช้ตัว `T` เพราะตั้งใจจะสื่อว่า ย่อมาจาก `Type`
- และโดย *Naming Convention ของ Generics* เราควรใช้เป็นตัวอักษร**พิมพ์ใหญ่ตัวเดียว**)

> คำอธิบายเพิ่มเติม:
>> - Generic type `T` ในฟังก์ชัน `addGeneric()`  
>> มี Function Type เป็น `(n1: T, n2: T) => T`  
>> หมายความว่า รับ parameter n1 และ n2 เป็น type ใด  
>> จะ return ออกมาเป็น type นั้นด้วย นั่นเองงงงง

## การใช้งาน Generic Types
เมื่อใช้งาน Generic Type เป็น Type ชนิดใดแล้ว  
เราจะต้องใช้เป็น Type นั้นไปจนจบ  
(Param หรือ ตัวแปรนั้นๆ จะเปลี่ยน Type ของ Generic ไม่ได้อีก) 

![ภาพแสดงตัวอย่างการใช้งาน Generic Types](images/008-02.png)

```typescript
// string[]
const cars: Array<string> = ['Toyota', 'Honda', 'Hyundai']

// (string | number | boolean)[]
const combined: Array<string | number | boolean> = [55, 'Haha', false, 'Run!']

type Person = { name: string, age: number }
// Person[]
const persons: Array<Person> = [
  { name: 'Jack Traven', age: 28 }
  { name: 'John Wick', age: 33 },
  { name: 'John Constantine', age: 32 }
]

const anyValues: Array<any> = ['Hello', 2, true, { name: 'Thomas Anderson', age: 30 }]
```
> เราอาจมองว่า Generic เป็นตัวแปรของ Type ก็ยังได้นะ  
> จาก code ตัวอย่างข้างล่างนี้ จะเห็นว่า `Array<T>` กลายเป็น `Array<string>`  
> เมื่อกรอกข้อมูลประเภท `string` เข้ามาใน Array  
> และจาก `Array<T>` กลายเป็น `Array<Person>`  
> เมื่อมีการใส่ข้อมูลที่เป็น instance ของ Person นั่นเองครับ  
> ##### ภาพแสดงตัวอย่าง Generic Type เมื่อใส่ข้อมูล `Person` ลงใน `Array`
> ![ภาพแสดงตัวอย่าง Generic Type เมื่อใส่ข้อมูล Person ลงใน Array](images/008-03.png)

## Create Generic Functions

### Function Declaration

```typescript
function combineValues<T>(val1: T, val2: T): T {
  if (typeof val1 === 'object' && typeof val2 === 'object') {
    return Object.assign(val1, val2)
  } else if (typeof val1 === 'string' && typeof val2 === 'string') {
    return val1 + val2
  } else if (typeof val1 === 'number' && typeof val2 === 'number') {
    return val1 + val2
  }
}

const newObject = combineValues<object>({ name: 'John', age: 33 }, { gender: 'male' })
const combinedStrings: string = combineValues<string>('Hello, ', 'world!')
const combinedNumbers = combineValues<number>(2, 5)

console.log(newObject)        // { name: 'John', age: 33, gender: 'male' }
console.log(combinedStrings)  // Hello, world!
console.log(combinedNumbers)  // 7
```
#### อีกสักตัวอย่าง
```typescript
function addItem<T>(list: Array<T>, item: T): Array<T> {
  return [...list, item]
}

console.log(addItem(persons, { name: 'Thomas Anderson', age: 30 }))
console.log(addItem(cars, 'Nissan'))    // ['Toyota', 'Honda', 'Hyundai', 'Nissan']
console.log(addItem([1, 2, 3, 4], 5))   // [1, 2, 3, 4, 5]
```

### Function Expression

```typescript
const addItem = <T>(list: Array<T>, item: T): Array<T> => {
  return [...list, item]
}

console.log(addItem(persons, { name: 'Thomas Anderson', age: 30 }))
console.log(addItem(cars, 'Nissan'))    // ['Toyota', 'Honda', 'Hyundai', 'Nissan']
console.log(addItem([1, 2, 3, 4], 5))   // [1, 2, 3, 4, 5]
```
เมื่อเอาเมาส์ชี้ไปที่ Generic Function ที่ถูกเรียกใช้  
จะเห็นได้ว่า Generic Type เป็น type อะไร  
ก็ขึ้นอยู่กับ Data type ของ argument ที่ส่งไปให้ function นั้น  
และ Data type นั้นจะถูกนำไป apply กับ type `T` ทั้งหมดที่กำหนดไว้ใน Function ด้วย  
![ภาพแสดงตัวอย่างการนำ Cursor ไปชี้ที่ Generic function ที่ถูกเรียกใช้งาน](images/008-04.png)

## Create Generic Classes
ก่อนที่จะอ่าน code ตัวอย่างต่อไปนี้ได้อย่างเข้าใจ  
อาจต้องศึกษาเรื่อง [OOP (Object-Oriented Programming)](09-oop.md) มาก่อน

```typescript
class Collection<T> {
  private items: Array<T>

  constructor(items: Array<T> = []) {
    this.items = items
  }

  toArray(): Array<T> {
    return this.items
  }

  add(item: T): this {
    this.items.push(item)
    return this
  }

  remove(item: T): this {
    this.items.splice(this.items.indexOf(item), 1)
    return this
  }
}

const cars: Collection<string> = new Collection()
cars.add('Toyota').add('Honda').add('Hyundai')
console.log(cars.toArray()) // ['Toyota', 'Honda', 'Hyundai']

cars.remove('Hyundai').add('Nissan').remove('Toyota')
console.log(cars.toArray()) // ['Honda', 'Nissan']

const oddsNumbers = findOddsFrom1To(100)
console.log(oddsNumbers)    // [1, 3, 5, ..., 99]

function findOddsFrom1To(n: number): Array<number> {
  const result: Collection<number> = new Collection()
  for (let i = 1; i <= n; i++) {
    if (i % 2 === 1) result.add(i)
  }
  return result.toArray()
}
```
---

<div align="center">
  <a href="07-function-as-type.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="09-oop.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

---

### Sources:
- [`<T>` และ `<E>` หรือ Generic ใน Java คืออะไร และใช้ยังไงกัน](https://www.tamemo.com/post/101/java-generic)
- [typescriptlang.org - Generics](https://www.typescriptlang.org/docs/handbook/2/generics.html)
