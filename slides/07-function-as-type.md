<div align="center">
  <a href="06-type-aliases-n-custom-type.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="08-generic-types.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Function as Type & void
> ### Table of Contents
>> - [Void](#void)
>> - [Function as Type](#function-as-type)
>> - [Pros of Function as Type](#ข้อดีของการใช้-function-as-type-ก็คือ)

## ก่อนเข้าเรื่อง Void และ Function as Type ขอกลับมาทบทวนเรื่องการประกาศฟังก์ชันแป๊ปนึง
### ส่วนประกอบของฟังก์ชันที่ประกาศแบบ Function Declaration
![ภาพแสดงคำอธิบายของ Function Declaration](images/007-01.png)

## Void
`void` เป็น return type ที่เป็นประเภทการคืนค่า (return) แบบไม่มีการคืนค่าใดๆ ออกจากฟังก์ชัน  
- มักใช้กับฟังก์ชันที่ไม่ต้องการนำค่าที่ประมวลผลแล้วไม่ต้องการนำผลลัพธ์มาใช้งานต่อ เช่น
   - ฟังก์ชันที่ใช้แสดงผลลัพธ์ออกทางหน้าจอ 
   - ฟังก์ชันที่มีการส่งค่าบางอย่างไปให้ฟังก์ชันอื่นทำงานต่อ
- `void` ใช้เป็น value type (type ของตัวแปร และ Parameter) มิได้

### ตัวอย่าง `void` function
```typescript
function add(num1: number, num2: number): number {
  return num1 + num2
}

function displayResult(result: any): void {
  console.log(`The result is ${result}`)
}

displayResult(2, 5) // The result is 7
```
จาก code นี้ `displayResult()` เป็น `void` function  
เพราะว่ามีการส่งค่า `result` param ไปให้ ฟังก์ชัน `log()` (ในคลาส `Console`) ทำงานต่อไปนั่นเอง

## Function as Type
นอกจากการกำหนด Primitive และ Reference type (ซึ่งผมขอเรียกมันว่า Value as Type) ได้แล้ว  
เรายังสามารถให้ type เป็น function (Function as Type) ได้ด้วยนะ  

### รูปแบบการกำหนด Function type
โดยทั่วไป เราสามารถกำหนด type `Function` ให้กับ Function Declaration ได้เลย ดังตัวอย่างนี้
```typescript
const add: Function = (num1: number, num2: number) => {
  return num1 + num2
}
```

ซึ่งก็เป็นท่าที่ยังไม่ดีเท่าไร เพราะ `Function` type ไม่ได้กำหนดโครงสร้างของฟังก์ชันที่ชัดเจน  
รวมถึงไม่ได้บอกเราว่าจะ return เป็น type อะไร  

นั่นจึงเป็นที่มาของการใช้ Function as type ดังที่จะอธิบายต่อข้างล่างนี้  

#### 
![ภาพแสดงคำอธิบาย Function as Type - 2](images/007-02.png)
![ภาพแสดงคำอธิบาย Function as Type - 1](images/007-03.png)

### ข้อดีของการใช้ Function as Type ก็คือ
- การใช้ Function as type แทน `Function` type ให้กับตัวแปรที่ถือค่า function ใดๆ
  จะช่วยให้เรามั่นใจในการนำตัวแปรดังกล่าวไปใช้งานมากขึ้น
  เพราะว่า Function as type ทำให้ตัวแปรนั้นเป็น function ที่มี Input(s) และ Output ในตัวมันเองแล้ว
- การนำไปใช้กับ Function as Parameter (callback function) ที่ param นั้นมีหน้าที่ชัดเจน  
  จะช่วยให้ Developer Experience ของผู้ที่เรียกใช้งานฟังก์ชันเราดีขึ้น


```typescript
function displayResult(result: any): void {
  console.log(`The result is ${result}`)
}

const add = (num1: number, num2: number) => {
  return num1 + num2
} // หรือ...
/** function add(num1: number, num2: number): number {
 *    return num1 + num2
 *  }
 */

function doCalculate(
  calculateFn: (num1: number, num2: number) => number,
  n1: number, n2: number
): number {
  return calculateFn(n1, n2)
}

displayResult(doCalculate(add, 5, 7)) // The result is 12
```

---

<div align="center">
  <a href="06-type-aliases-n-custom-type.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="08-generic-types.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />
