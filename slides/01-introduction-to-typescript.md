<div align="center">
  <a href="#">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="02-setting-up-typescript.md">
    หน้าต่อไป ▶ 
  </a>
</div>

---

# Introduction to Typescript

## Typescript

**Typescript** เป็นภาษาที่ถูกสร้างขึ้นโดย Microsoft  
เพื่อมาเพิ่มขีดความสามารถที่ไม่มีใน Javascript  
อย่างเช่น การกำหนดชนิดของข้อมูล (Dynamic type)  
รวมไปถึง Feature อื่นๆ อีกหลายอย่างที่ Javascript ไม่มี  
ทำให้ Code ที่เขียนด้วยภาษา Typescript มี bug น้อยกว่า  
และยังให้ประสบการณ์ที่ดีแก่ Developer (Developer Experience) มากกว่าอีกด้วย

### ข้อดีของ Typescript

- มีความชัดเจนเรื่องชนิดของข้อมูล (Static type)
- Javascript ทำอะไรได้ Typescript ก็ทำได้เช่นกัน
- รองรับ Javascript Syntax สมัยใหม่ (ES2015 หรือ ES6 ขึ้นไป)  
`แม้ว่าบาง Browser จะยังไม่รองรับ ES6 แต่ Typescript compiler จะ compile ไปเป็น syntax ES5 เพื่อให้ Browser ทำงานได้อยู่ดี`
- สามารถแสดง Error บางชนิดได้ตั้งแต่ Compile time
- สามารถพัฒนา web application ได้ทั้งฝั่ง Frontend และ Backend เช่นเดียวกับ Javascript

> - *ข้อดีอีกบางส่วนที่ผมได้เขียนไว้ในบทความ [>> 3 ขั้นตอน การสร้าง Web Server ด้วย NodeJS +Typescript + Express <<](http://medium.com/p/ba8176b22809#c34c) (อยู่ในช่วงต้นๆ ของบทความ)*
> - [>> คำตอบของคุณ Lodewijk Bogaards ใน Stackoverflow <<][Lodewijk Bogaards's answer] เรื่องข้อดีของ Typescript (เขียนตอบไว้ละเอียดมาก)

### ตัวอย่าง Feature บางส่วนที่มีใน TypeScript (แต่ Javascript ไม่มี)

- Abstract classes
- Interfaces
- Enums
- Decorator
- Generics
- Namespaces
- etc..

### ข้อแตกต่างระหว่าง JavaScript กับ TypeScript

- **JavaScript** เป็นภาษาที่สามารถเรียนรู้ได้ง่าย  
  ในขณะที่ **TypeScript** ต้องอาศัยการเรียนรู้ที่มากกว่า และอาจต้องรู้จัก Javascript มาบ้าง  
  (แต่จะเริ่มศึกษา Typescript ก่อนเลยก็ทำได้เช่นกัน)
- **TypeScript** *รองรับ* Static Typing ซึ่งทำให้เราสามารถตรวจสอบความถูกต้องของ type ได้ในระหว่างที่ Compile  
  แต่ในขณะที่ **JavaScript** *ไม่รองรับ*สิ่งนี้
- Code ของ **TypeScript** จำเป็นต้องถูก Compile ก่อน แล้วจึงนำไป Run  
  แต่สำหรับ **JavaScript** ไม่ต้อง Compile ก่อน Run .

#### ปุจฉา...

เราจะตรวจสอบ type ของ `num1` กับ `num2` เพื่อให้มั่นใจว่าทั้งสองค่าบวกกันแล้วได้ผลลัพธ์ออกมาได้อย่าง**ถูกต้องตามหลักการบวกเลข**ได้อย่างไร ?
(`1 + 1` ต้องเท่ากับ `2` ไม่ใช่ `11`)

```javascript
function add(num1, num2) {
  return num1 + num2
}
```

.

#### วิสัจชนา...

ก็เช็ค type ด้วย keyword `typeof` ไง

```javascript
function add(num1, num2) {
  if (typeof num1 !== 'number' || typeof num2 !== 'number')
    throw new Error('num1 or num2 is not number type')

  return num1 + num2
}
```

.

#### แต่เดี๋ยวก่อน เราขอเสนอ Typescript โดยเราจะกำหนด type ให้กับ `num1` และ `num2` ได้เลยตั้งแต่ต้น (และในกรณีนี้เราไม่ต้องมาเช็ค `typeof` ซ้ำอีก)

```typescript
function add(num1: number, num2: number): number {
  return num1 + num2
}
```

#### ตัวอย่าง Code ที่แสดงความแตกต่างระหว่าง Typescript และ Javascript
![ภาพแสดง code ที่แตกต่างระหว่าง Typescript และ Javascript](./images/001-01.png)

<details><summary><b>Typescript</b></summary>

```typescript
type NumLegs = 1 | 2 | 4 | 6 | 8 | 'much'

interface Animal {
  numLegs: NumLegs
  hasWing: boolean
  eat(food: string): void
}

abstract class FlyableAnimal implements Animal{
  abstract numLegs: NumLegs
  hasWing: boolean = true

  constructor(hasWing?: boolean) {
    this.hasWing = hasWing || false
  }

  eat(food: string): void {
    console.log(`I eat ${food}.`)
  }
}

abstract class UnflyableAnimal implements Animal {
  abstract numLegs: NumLegs
  hasWing: boolean = false

  eat(food: string): void {
    console.log(`I eat ${food}.`)
  }
}

class Bird extends FlyableAnimal {
  numLegs: NumLegs
  name: string

  constructor(name: string) {
    super()
    this.name = name
    this.numLegs = 2
  }


  eat(food: string): void {
    console.log(`${this.name} eats ${food}.`)
  }
}

class Duck extends FlyableAnimal {
  numLegs: NumLegs
  
  constructor(public name: string) {
    super(false)
    this.numLegs = 2
  }
}

const myBird = new Bird('Woody')
myBird.eat('corn')
console.log(`${myBird.name} has ${myBird.numLegs} legs.`)

const myDuck = new Duck('Donald')
myDuck.eat('fish')

```

</details>


<details><summary><b>Output (Javascript)</b></summary>

```ts
"use strict";
class FlyableAnimal {
    constructor(hasWing) {
        this.hasWing = true;
        this.hasWing = hasWing || false;
    }
    eat(food) {
        console.log(`I eat ${food}.`);
    }
}
class UnflyableAnimal {
    constructor() {
        this.hasWing = true;
    }
    eat(food) {
        console.log(`I eat ${food}.`);
    }
}
class Bird extends FlyableAnimal {
    constructor(name) {
        super();
        this.name = name;
        this.numLegs = 2;
    }
    eat(food) {
        console.log(`${this.name} eats ${food}.`);
    }
}
class Duck extends FlyableAnimal {
    constructor(name) {
        super(false);
        this.name = name;
        this.numLegs = 2;
    }
}
const myBird = new Bird('Woody');
myBird.eat('corn');
console.log(`${myBird.name} has ${myBird.numLegs} legs.`);
const myDuck = new Duck('Donald');
myDuck.eat('fish');

```


</details>


<details><summary><b>Compiler Options</b></summary>

```json
{
  "compilerOptions": {
    "noImplicitAny": true,
    "strictNullChecks": true,
    "strictFunctionTypes": true,
    "strictPropertyInitialization": true,
    "strictBindCallApply": true,
    "noImplicitThis": true,
    "noImplicitReturns": true,
    "alwaysStrict": true,
    "esModuleInterop": true,
    "declaration": true,
    "experimentalDecorators": true,
    "emitDecoratorMetadata": true,
    "target": "ES2015",
    "jsx": "react",
    "module": "ESNext",
    "moduleResolution": "node"
  }
}
```


</details>

[**>> ถ้าอยากลอง Run code ด้วยตนเอง ให้จิ้มตรงนี้แรงๆ <<**](https://www.typescriptlang.org/play?target=2#code/C4TwDgpgBAcgrgWwDIQOYGcoF4oEYoA+UATIVACxkBsZAHGQOQJwDGAFgwFCcCWAdsAgAnAGYBDFtACCfHgjEAbKAG9OUKH0QoMALlha06NVDZj0AdX6o9AIwD2dhRDF9jz4AAoRDgCZ70wEJWAJR6AG52PD6cAL7cYjYBQhLAUCwKZpgAYgogCU4ycopQcmBOCBACmIXySqrqCUkpGga6+siGxqYWVrYOTi7YUIFwENzqLHZ8SazAdkIe3ZZ8qAD8fY7OfMEqxurAbDzoAHRLVkNnK4RE4groY+pxbmKe3nZ+UEkh4ZE+u+oTKboTbHBR2VAeAAGAEkoO4oAASZRvHwxY6Q4LGOJPRqBZrpTJQACqfBEuXyEBqxVK5UqwGqslq-yguOSLFSmg6bXgXKM6ku1ig9k2gxwt3u4zhLy8vn8gW+UAiUWZgOmILBEJhUtSSJRaIxWNi3AJ6EwACEeEI-hAAB6CPg+bLkmwFRnFeotXl6HnaPkaMQVOVBFaSybTEbs+YePgBiBBkIqz5wSALTEA4aHE4xipDbMPAEHI7HTm+obEQ2S9wy97xlahRW-RNh4FOUHgqFIwtZ2MxbWYXW+fVpqDY40ZU1QAAirAA1nC7ZVHVAcnkXZS3XVjCXDN7Wn7jM2I3MFmA4C6eCx-YHPvK64n0MnhF5FPdh-tM8W92XDU9D1AECAFpWrmEAAO5QEBPgeAw5i+CADCYgBkHHFWDCTEIfAIZwzbqu2kJIkhlo+MWPYmGYiLKIRVqfryvZOBg6KYthQKpAB04sHOOB8GBU6ztBk5TIoPhYWxs4odKDAiEcHCYkAA)
     

##### ข้อสังเกต จาก code Typescript (ด้านซ้าย) Javascript (ด้านขวา) ในภาพ
- กรอบสีแดงที่ผมทำไว้ คือ code Typescript ที่จะไม่ถูก compile ไปเป็น Javascript ซึ่งได้แก่
  1. Type alias
  2. Abstraction (Abstract class, Abstract property, Abstract method และ Interface)  
   เพราะว่า Javascript ไม่มีเรื่องของการระบุ Type รวมถึงการตั้งชื่อเล่นให้ Type และไม่มีเรื่องของ Abstraction Layer

---
Introduction to Typescript ขอจบลงแต่เพียงเท่านี้  
ต่อไปเราจะมา [เริ่ม Setup Typescript กัน](02-setting-up-typescript.md)

---

<div align="center">
  <a href="#">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="02-setting-up-typescript.md">
    หน้าต่อไป ▶ 
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />

## Sources:

- [ข้อแตกต่างระหว่าง TypeScript กับ JavaScript Programming](https://www.techstarthailand.com/blog/detail/Difference-Between-TypeScript-and-JavaScript-Programming/906)
- [What is TypeScript and why would I use it in place of JavaScript? - StackOverflow][Lodewijk Bogaards's answer]


[Lodewijk Bogaards's answer]: https://stackoverflow.com/questions/12694530/what-is-typescript-and-why-would-i-use-it-in-place-of-javascript/35048303#35048303
