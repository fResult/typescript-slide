<div align="center">
  <a href="02-setting-up-typescript.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="04-basic-types.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Type Using & Type Casting
> ### Table of Contents
>> - [Type Using](#type-using)
>> - [Type Casting](#type-casting)

## Type Using
### การกำหนด type ให้กับตัวแปร (variable)
![ภาพแสดงคำอธิบายเรื่องการประกาศตัวแปร](images/003-01.png)

1. การกำหนด type ให้กับตัวแปร Statement ในการกำหนด type เมื่อประกาศตัวแปร
   ```typescript
   // แบบประกาศให้เป็นตัวแปรค่าคงที่
   const <variable_name>: <data_type> = <value>
   // เช่น
   const nickName: string = 'Korn' 

   // แบบประกาศตัวแปรแบบไม่เป็นค่าคงที่ และ assign value ทันที
   let <variable_name>: <data_type> = <value>
   // เช่น
   let nickName: string = 'Korn'

   // แบบประกาศตัวแปรแบบไม่เป็นค่าคงที่ และเป็นแบบ assign value ในภายหลัง
   let <variable_name>: <data_type>
   <variable_name> = <value>
   // เช่น
   let nickName: string
   nickName =  'Korn'
   ```

   > ### **Note that...**
   >> - เมื่อประกาศตัวแปร พร้อมกับ assign value ใดๆ  แล้วตัวแปรนั้นๆ จะมี type เช่นเดียวกับ data type ของ value นั้นไปโดยปริยาย  
   >> - การประกาศตัวแปรด้วย `const` พร้อม assign value ทันที อาจละการกำหนด type ไว้ได้
   >> - การประกาศตัวแปรด้วย `let` พร้อม assign value ทันที หรือไว้ assign value ในภายหลัง **ไม่ควร**ละการกำหนด type เพราะว่า ตัวแปรนั้นๆ อาจถูกนำไป assign value ที่มี type แบบอื่น


### การกำหนด type ให้กับฟังก์ชัน (function)
#### Function Declaration
![ภาพแสดงคำอธิบายเรื่องการประกาศฟังก์ชัน แบบ declaration](images/003-02.png)

#### Function Expression
![ภาพแสดงคำอธิบายเรื่องการประกาศฟังก์ชัน แบบ expression](images/003-03.png)

### ลองหน่อย... [2]
#### 1. ให้สร้างฟังก์ชันตรวจสอบว่า string 2 ค่าที่ป้อนเข้ามาว่าจะมีค่าเท่ากันหรือไม่
- Input: `isEqualText('Hello','Hello')`  
  Output: `true`
- Input: `isEqualText('Goodbye','Goodnight')`  
  Output: `false`


## Type Casting
**Type casting** คือการแปลงชนิดของข้อมูล จากชนิดข้อมูลที่ไม่ชัดเจนให้มีความเฉพาะมากขึ้น
การแปลงชนิดข้อมูลนี้ ทำได้โดย 2 กรณี (เท่าที่ผมรู้ตอนนี้)
1. การแปลงจากตัวแปรหรือ parameter ที่มีมากกว่า 1 type ให้เหลือ type เดียว
2. การแปลงตัวแปรหรือ parameter ที่มี type ลำดับสูงกว่า ให้เป็น type ที่ต่ำกว่าตามลำดับชั้นใน Class Hierarchy

#### **ภาพแสดง Class Hierarchy ของ TypeScript**
![ภาพแสดง Class Hierarchy ของ TypeScript](images/003-04.png)

### ตัวอย่าง Type Casting
#### **ภาพแสดง Class Hierarchy จากคลาสที่สร้างขึ้นเอง**
![ภาพแสดง Class Hierarchy](images/003-05.png)

```typescript
class Animal {
  protected legs: number
  protected canFly: boolean

  constructor(legs: number, canFly: boolean) {
    this.legs = legs
    this.canFly = canFly
  }

  eat(thing: any): void {
    console.log(`I am eating ${thing}`)
  }

  walk(): void {
    console.log(`I am walk walking`)
  }
}

class Human extends Animal {
  private arms: number

  constructor(arms: number, legs: number, canFly: boolean) {
    super(legs, canFly)
    this.arms = arms
  }

  talk(): void {
    console.log('I am talking')
  }
}

class Bird extends Animal {
  wings: number

  constructor(wings: number, legs: number, canFly: boolean) {
    super(legs, canFly)
    this.wings = wings
  }
  
  fly(): void {
    console.log('I am flying')
  }
}

class Cat extends Animal {
  constructor() {
    super()
  }
}


const myAnimal1: Animal = new Bird(/*wings=*/2, /*legs=*/2, /*canFly=*/true);
console.log(myAnimal1);
```
จาก code ข้างบน ที่ 2 บรรทัดสุดท้าย เราสร้าง instance `myBird` จากคลาส `Bird` ให้เป็น type `Animal`  
เราสามารถ Log `myBird` ออกมาได้ข้อความว่า `Bird { legs: 2, canFly: true, wings: 2 }`

และเมื่อเราเรียกใช้ method `fly()` ด้วย `myBird.fly()` จะพบว่า syntax error  
เพราะว่าตัวแปร `myBird` เป็น type `Animal` ซึ่งไม่มี method `fly()`  
ทีนี้เราต้องทำยังไงล่ะ ?

คำตอบก็คือ การ cast type จาก `Animal` ให้เป็น `Bird` นั่นเอง
#### โดยวิธีการ cast type ในภาษา Typescript มีด้วยกัน 2 ท่าครับ
1. `const ชื่อตัวแปร = <ชนิดข้อมูลปลายทาง> ชื่อตัวแปรของอินสแตนซ์เดิม` เช่น `const myBird = <Bird>myAnimal1`
2. `const ชื่อตัวแปร = ชื่อตัวแปรของอินสแตนซ์เดิม as ชนิดข้อมูลปลายทาง` เช่น `const myBird = myAnimal1 as Bird`

ชอบท่าไหนก็เลือกกันเอาเองนะครับ

#### เมื่อ cast type จาก `Animal` เป็น `Bird` เราสามารถเรียกใช้ method `fly()` ได้แล้ว
```typescript
const myBird = <Bird> myAnimal
console.log(myBird.fly())   // I'm flying
```

### ลองหน่อย... [3]
#### 1. ให้ลอง copy code `index.html` และ `index.ts` ไปลองเอง โดยตัด code ส่วนที่เป็น Type Casting ออก แล้วสังเกตดูผลลัพธ์ว่าเกิดอะไรขึ้น
เช่น `const formElem = <HTMLFormElement>document.querySelector('form')`  
ให้ตัด `<HTMLFormElement>` ออกจนเหลือแค่ `const formElem = document.querySelector('form')`
```html
<!-- index.html -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta
    name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
  />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <title>Todo List</title>
</head>
<body>
<script defer src="index.js"></script>
<form>
  <label for="text">Task</label>
  <input type="text" id="text" />
  <br />
  <button type="submit">Add New Task</button>
</form>
<h3>Todo List</h3>
<ul>
  <li>Read some books</li>
  <li>Exam</li>
</ul>
</body>
</html>
```

```typescript
// index.ts
const formElem = <HTMLFormElement>document.querySelector('form')
const inputElem = <HTMLInputElement>document.querySelector('#text')

const ulElem = <HTMLUListElement>document.querySelector('ul')

formElem.addEventListener('submit', (e: Event) => {
  e.preventDefault()
  const liElem = document.createElement('li')
  liElem.innerHTML = inputElem.value
  ulElem.appendChild(liElem)
  inputElem.value = ''
})
```

---

<br />

<div align="center">
  <a href="02-setting-up-typescript.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="04-basic-types.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />

## Sources:

- [Typescript - Basic Types](https://www.typescriptlang.org/docs/handbook/basic-types.html)
