<div align="center">
  <a href="03-type-using-n-casting.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="05-union-types.md">
    หน้าต่อไป ▶
  </a>
</div>

---

# Basic Types

## Primitive Types

|  **Type**    |     **Example data**          |         **Description**                    |
|:------------:| ----------------------------- |:------------------------------------------ |
| **number**   | `5`, `3.5`, `-1.543`          | All number values both intergers or floats |
| **string**   | `"Hello, World"`, `"Say hi"`  | All text values                            |
| **boolean**  | `true`, `false`               | Just *truthy* / *falsy* values             |

#### ในไฟล์ `main.ts` เรามาลองเขียนแบบไม่กำหนด type เพื่อจำลองว่าเป็นการเขียนแบบ `js` กันอีกครั้ง

1. ใส่ code ลงในไฟล์ `main.ts`
   ```javascript
   function add(num1, num2) {
      return num1 + num2
   }

   const n1 = '12'
   const n2 = 8.5

   /**
    *  Several lines executing another code.
    *  And then call `add()` function
    */
   console.log('The result is:', add(n1, n2))
   ```
2. รันคำสั่ง `tsc main.ts`  แล้วรัน `nodemon main.js` จะได้ผลลัพธ์คือ `128.5` แทนที่จะเป็น `20.5` ตามความคาดหวัง
   ![ภาพแสดงผลการรันไฟล์ main.js ที่คอมไพล์แล้ว](images/004-01.png)

3. เราแก้ไขได้โดยการกำหนด type ที่แน่นอนให้แก่ parameter `num1` และ `num2` เพื่อให้แน่ใจได้ว่า ทั้ง 2 param จะมีค่าเป็น
   number อยู่เสมอ
   ```typescript
   function add(num1: number, num2: number) {
     return num1 + num2;
   }

   const n1 = '12';
   const n2 = 8.5;

   console.log("The result is:", add(n1, n2));
   ```
  1. `tsc main.ts` เพื่อคอมไพล์ `ts` เป็น `js` จะเห็นได้ว่า เราคอมไพล์ไม่ผ่าน เนื่องจากใส่ argument `n1` เป็น string แต่
     function required number
     ![ภาพแสดงผลการ compile ts (ได้ error)](images/004-02.png)
  2. เมื่อเราแก้จาก `const n1 = '12'` เป็น `const n1 = 12` เราจึงจะ compile และดูผลลัพธ์จากการรัน `nodemon main.js`
     ได้ `20.5` ตามที่เราคาดหวังครับ

   > เห็นแล้วใช่ไหมครับว่า feature การกำหนด type ของภาษา typescript มัน powerful ขนาดไหนกัน

> ### Note that...
>> - Javascript จะรู้ error แบบนี้ตอน runtime ในขณะที่ Typescript จะรู้ error แบบนี้ตั้งแต่ตอน compile ทำให้โอกาสเกิด Bug จากการใช้ข้อมูลผิด type กลายเป็นศูนย์
>>   - type `string` และ class `String` ไม่ใช่อันเดียวกัน (ขอให้ระวังสับสนในการนำไปใช้)
>>   - type `number` และ class `Number` ก็ไม่ใช่อันเดียวกัน

## Reference Types

|  **Type**    |     **Example data**             |       **Description**       |
|:------------:| -------------------------------- |:--------------------------- |
| **object**   | `{ firstName: 'Sila' }`          | Any Javascript object       |
| **Array**    | `["Korn", "Bas", "Beer", "Guy"]` | Any JavaScript array        |
| **Tuple**    | `[1, "Hello", true]`             | **Tuple types allow you to express an array with a fixed number of elements** <br/> whose types are known, but need not be the same. <br/> For example, you may want to represent a value as a pair of a string and a number |

### การประกาศตัวแปร แบบ Object type
![ภาพแสดงคำอธิบายการประกาศตัวแปรแบบ Object type](images/004-04.png)

#### ทดสอบ object type
1. สร้างไฟล์ ใหม่ชื่อ `reference-types.ts`
2. ในไฟล์ดังกล่าวให้ใส่ code ดังต่อไปนี้
   ```typescript
   const person: { nickName: string, age: number } = {
     nickName: 'Korn', 
     age: 18
   }
   
   console.log(person.nickName) // 'Korn'
   console.log(person.age)      // 18
   ```
3. compile`reference-type.ts`  และ run ไฟล์ `reference-type.js`

```npm
tsc reference-types.ts && nodemon reference-types.js
```

4. ลองเพิ่ม code `console.log(person.title)` ลงในบรรทัดใหม่ของไฟล์ `reference-types.ts` จากนั้น ให้ compile + run อีกที
   ![ภาพแสดงผลลัพธ์การ compile ไม่ผ่าน เนื่องจาก title ไม่ได้มีอยู่ใน Person](images/004-03.png)  
   เราจะเห็นได้ว่า compile ไม่ผ่าน ด้วย error ที่ว่า `title` ไม่มีอยู่ใน `Person` type ที่เราสร้างไว้
5. ลองเสร็จแล้วลบบรรทัดที่เขียนขึ้นในข้อ 4 ออกด้วยนะ

### การประกาศตัวแปร แบบ Array type
![ภาพแสดงคำอธิบายการประกาศตัวแปรแบบ Array type](images/004-05.png)

#### ทดสอบ array type
1. ในไฟล์ `reference-types.ts` เพิ่ม code ตามนี้
   ```typescript
   const pets: Array<string> = ['Dog', 'Cat', 'Rat', 'Bird']
   ```
2. compile`reference-type.ts`  และ run ไฟล์ `reference-type.js`
   ```npm
   tsc reference-types.ts && nodemon reference-types.js
   ```
3. ลองเพิ่ม code `pets.push(123)` ลงในบรรทัดใหม่ของไฟล์ `reference-types.ts` จากนั้น ให้ compile + run อีกที
   ![ภาพแสดงผลลัพธ์การ compile ไม่ผ่าน เนื่องจาก `123` ไม่ได้มีอยู่ใน ไม่ใช่ string](images/004-06.png)  
   ผลลัพธ์ก็คือ การ compile ไม่ผ่าน ด้วย error ที่ว่า `Argument of type 'number' is not assignable to parameter of type 'string'.`
4. เมื่อลองเสร็จแล้ว ให้ลบบรรทัดที่เขียนขึ้นในข้อ 3 ออกด้วย
## ลองหน่อย...
### 1. จากเรื่อง `ทดสอบ object types` ให้ลอง แก้ไขข้อมูล `nickName` ของ object `person` เป็น `007` (`person.nickName = 007`) แล้วดูว่าผลของการ compile + run เป็นอย่างไร
### 2. จากเรื่อง `ทดสอบ Array types` ให้ลอง แก้ไขข้อมูลข้อมูลลงใน index ที่ 3 เป็น `true` (`pets[3] = true`) แล้วดูผลของการ compile + run แล้วเป็นอย่างไร

## Any Type
|  **Type**    |     **Example data**             |       **Description**       |
|:------------:| -------------------------------- |:--------------------------- |
| **any**      | `"string"`, `1`, `true`,<br/> `{ key: "value" }`, `[]`<br/> (หรือจะเป็นข้อมูลชนิดใดๆ ก็ได้)       | Any Javascript types       |
1. any type

### การประกาศตัวแปร แบบ any type
![ภาพแสดงคำอธิบายการประกาศตัวแปรแบบ any type](images/004-07.png)

#### ทดสอบ any type
1. ในไฟล์ `reference-types.ts` ให้ประกาศตัวแปรเป็น type any
2. ใส่ข้อมูลอะไรก็ได้

---

## ลองหน่อย...
### 1. จากเรื่อง `ทดสอบ object types` ให้ลอง แก้ไขข้อมูล `nickName` ของ object `person` เป็น `007` (`person.nickName = 007`) แล้วดูว่าผลของการ compile + run เป็นอย่างไร
### 2. จากเรื่อง `ทดสอบ Array types` ให้ลอง แก้ไขข้อมูลข้อมูลลงใน index ที่ 3 เป็น `true` (`pets[3] = true`) แล้วดูผลของการ compile + run แล้วเป็นอย่างไร

---

<div align="center">
  <a href="03-type-using-n-casting.md">
    ◀ ไปก่อนหน้า
  </a>
  &nbsp;|&nbsp;
  <a href="05-union-types.md">
    หน้าต่อไป ▶
  </a>
</div>

<br />

<div align="center">
  <a href="../README.md" style="font-size: 24px;">
    &gt;&gt;&gt; ไปหน้าสารบัญ &lt;&lt;&lt;
  </a>
</div>

<br />
<hr />

## Sources:

- [Typescript - Basic Types](https://www.typescriptlang.org/docs/handbook/basic-types.html)
